import 'package:discountella/controllers/list_controller.dart';
import 'package:discountella/models/product.dart';
import 'package:discountella/services/product_service.dart';
import 'package:get/get.dart';

class ListDetailController extends GetxController {
  RxList<Product> productList = RxList<Product>();
  Category activeCategory;
  List<Product> get products => productList.value;
  bool _loading = true;

  bool get loading => _loading;

  @override
  void onInit() async {
    super.onInit();
    _loading = false;
    productList.bindStream(ProductService().listProduct(
        Get.find<ListController>()
            .lista
            .products
            .map((e) => e as String)
            .toList()));
    update();
  }
}
