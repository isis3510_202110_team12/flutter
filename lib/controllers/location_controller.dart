import 'dart:async';
import 'package:background_location/background_location.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:discountella/services/analytics_service.dart';
import 'package:discountella/services/market_service.dart';
import 'package:get/state_manager.dart';

class LocationController extends GetxController {
  final AnalyticsService _analyticsService = AnalyticsService();
  var pLatitude = 0.0.obs;
  var pLongitude = 0.0.obs;
  var latitude = ''.obs;
  var longitude = ''.obs;
  var name = ''.obs;

  notification() async {
    //await BackgroundLocation.setAndroidConfiguration(1000);
    await BackgroundLocation.startLocationService(distanceFilter: 20);

    BackgroundLocation.getLocationUpdates((location) {
      pLatitude.value = location.latitude;
      pLongitude.value = location.longitude;
      latitude.value = location.latitude.toString();
      longitude.value = location.longitude.toString();
      print('''\n
                        Latitude:  $latitude
                        Longitude: $longitude
                      ''');
      Timer.periodic(Duration(seconds: 90), (timer) {
        _analyticsService.logContextAware();
        MarketService()
            .marketNotification(pLatitude.value, pLongitude.value)
            .listen((List<DocumentSnapshot> documentList) async {
          if (documentList.length > 0) {
            name(documentList[0]['name']);
            notficationNearby();
          } else {
            notficationNotNearby();
          }
        });
      });
    });
  }

  notficationNearby() async {
    _analyticsService.logNearbyMarkets();
    await BackgroundLocation.setAndroidNotification(
      title: 'Cerca de: ' + name.value,
      message: 'Mira las promociones',
      icon: '@mipmap/ic_launcher',
    );
  }

  notficationNotNearby() async {
    await BackgroundLocation.setAndroidNotification(
      title: 'Servicio Background',
      message: '',
      icon: '@mipmap/ic_launcher',
    );
  }
}
