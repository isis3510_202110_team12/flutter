import 'package:discountella/models/user.dart';
import 'package:get/state_manager.dart';

class UserController extends GetxController {
  Rx<UserData> _userModel = UserData().obs;

  UserData get user => _userModel.value;

  set user(UserData value) => this._userModel.value = value;

  void clear() {
    _userModel.value = UserData();
  }
}
