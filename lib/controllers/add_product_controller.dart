import 'package:discountella/services/analytics_service.dart';
import 'package:discountella/services/product_service.dart';
import 'package:discountella/models/product.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart' as path;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:io';

class AddProductController extends GetxController {
  final AnalyticsService _analyticsService = AnalyticsService();
  final formKey = GlobalKey<FormState>();
  final amountController = TextEditingController();
  final brandController = TextEditingController();
  final imageController = TextEditingController();
  final nameController = TextEditingController();

  File image;

  String unitController;
  bool success;

  bool _loading = true;
  bool get loading => _loading;

  void dispose() {
    // Clean up the controller when the Widget is disposed
    amountController.clear();
    brandController.clear();
    imageController.clear();
    nameController.clear();
    super.dispose();
  }

  @override
  void onInit() {
    super.onInit();
    _loading = false;
  }

  void addProduct() async {
    _loading = true;
    try {
      final Product product = new Product(int.parse(amountController.text),
          brandController.text, nameController.text, unitController);

      if (await ProductService().existProduct(
          nameController.text, brandController.text, unitController)) {
        print('Ya existe un producto con este nombre, marca y unidades');

        Get.snackbar(
            'Fallo', 'Ya existe un producto con este nombre, marca y unidades',
            snackPosition: SnackPosition.TOP);
      } else {
        if (await ProductService().addProduct(product, image)) {
          await _analyticsService.logProductosCreados( producto: product);
          _loading = false;
          success = true;
          print('Nuevo producto registrado con éxito');
          Get.snackbar('Éxito', 'producto agregado con éxito');
          dispose();
        } else {
          print('No se registro el producto correctamente');
          Get.snackbar('Éxito', 'producto agregado con éxito',
              snackPosition: SnackPosition.TOP);
        }
      }
    } catch (e) {
      print(e);
      _loading = false;
      Get.snackbar('Fallo', 'No puede agregar el producto, revise',
          snackPosition: SnackPosition.TOP);
    }
  }

  void uploadImage() async {
    final picker = ImagePicker();
    PickedFile pickedImage;
    try {
      pickedImage =
          await picker.getImage(source: ImageSource.gallery, maxWidth: 1920);

      imageController.text = path.basename(pickedImage.path);
      image = File(pickedImage.path);
    } catch (e) {
      print(e);
      Get.snackbar('Fallo', 'No puede cargar la imagen, revise',
          snackPosition: SnackPosition.TOP);
    }
  }
}
