import 'package:discountella/controllers/login_controller.dart';
import 'package:discountella/models/lista.dart';
import 'package:discountella/services/analytics_service.dart';
import 'package:discountella/services/list_service.dart';
import 'package:discountella/services/user_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class AddListController extends GetxController {
  final AnalyticsService _analyticsService = AnalyticsService();
  final formKey = GlobalKey<FormState>();
  final titleController = TextEditingController();

  final loginController = Get.find<LoginController>();

  bool success;

  bool _loading = true;

  bool get loading => _loading;

  void dispose() {
    titleController.clear();
    super.dispose();
  }

  @override
  void onInit() {
    super.onInit();
    _loading = false;
  }

  void addList() async {
    _loading = true;
    try {
      final now = new DateTime.now();
      String formatter = DateFormat('yyyy.MM.dd hh:mm').format(now);
      String userId =
          await UserServices().getIdUserByEmail(loginController.user.email);

      final Lista lista = new Lista(
          date: formatter,
          idUser: userId,
          products: [],
          title: titleController.text);

      if (await ListService().addList(lista)) {
        _loading = false;
        success = true;
        _analyticsService.logAgregarLista();
        print('Nueva lista registrado con éxito');
        Get.snackbar('Éxito', 'lista agregada con éxito');
        dispose();
      } else {
        print('No se registro la lista correctamente');
        Get.snackbar('Éxito', 'lista agregada con éxito',
            snackPosition: SnackPosition.TOP);

        if (await ListService().existList(titleController.text, userId)) {
          print('Ya existe una lista con este título');
          Get.snackbar('Fallo', 'Ya existe una lista con este título',
              snackPosition: SnackPosition.TOP);
        } else {
          if (await ListService().addList(lista)) {
            _loading = false;
            success = true;
            print('Nueva lista registrado con éxito');
            Get.snackbar('Éxito', 'lista agregada con éxito');
            dispose();
          } else {
            print('No se registro la lista correctamente');
            Get.snackbar('Fallo', 'No se registro la lista correctamente',
                snackPosition: SnackPosition.TOP);
          }
        }
      }
    } catch (e) {
      print(e);
      _loading = false;
      Get.snackbar('Fallo', 'No puede agregar la lista, revise',
          snackPosition: SnackPosition.TOP);
    }
  }
}
