import 'package:discountella/controllers/user_controller.dart';
import 'package:discountella/services/analytics_service.dart';
import 'package:get/get.dart';
import '../models/lista.dart';
import '../models/product.dart';
import '../services/list_service.dart';
import '../services/product_service.dart';
import 'package:http/http.dart' as http;

class ListController extends GetxController {
  final AnalyticsService _analyticsService = AnalyticsService();

  Rx<Lista> _lista = Lista().obs;
  Lista get lista => _lista.value;
  set lista(Lista value) => this._lista.value = value;

  RxList<Lista> _listaList = RxList<Lista>();
  List<Lista> get listas => _listaList.value;

  RxBool isAddingProduct = false.obs;

  bool _loading = true;
  bool get loading => _loading;

  @override
  void onInit() {
    super.onInit();
    _loading = false;
    _listaList.bindStream(
        ListService().listStream(Get.find<UserController>().user.uid));
    update();
  }

  @override
  void onReady() {
    _listaList.bindStream(
        ListService().listStream(Get.find<UserController>().user.uid));
  }

  addProduct(String uidProduct) async {
    try {
      isAddingProduct.value = true;
      List newProduct = lista.products;
      newProduct.add(uidProduct);
      Lista _list = Lista(
        date: lista.date,
        idUser: lista.idUser,
        title: lista.title,
        products: newProduct,
      );
      await ListService().addProduct(lista.uid, _list);
      lista = _list;
      Product _producto = await ProductService().getProduct(uidProduct);
      _analyticsService.logProductosagragadosLista(producto: _producto);
      Get.snackbar('Discountella', 'Se agrego el producto');
      isAddingProduct.value = false;
    } catch (err) {
      isAddingProduct.value = false;
      print(err);
    }
  }

  deleteList(String id) async {
    try {
      await ListService().deleteOne(id);
      int index = _listaList.value.indexWhere((element) => element.uid == id);
      _listaList.removeAt(index);
      Get.snackbar("Success", "Deleted", snackPosition: SnackPosition.BOTTOM);
    } catch (e) {
      print(e);
    }
  }

  Future<String> getSuggest(List<dynamic> products) async {
    var client = http.Client();
    try {
      var bodyProducts = [];
      products.forEach((element) {
        bodyProducts.add(addQuotes(element));
      });
      print(bodyProducts.toString());
      var response = await client.post(
          Uri.parse('https://api.authenticdemo.tk/api/v1/suggest'),
          body: bodyProducts.toString());
      return response.body;
    } finally {
      client.close();
    }
  }

  String addQuotes(String id) {
    return "'$id'";
  }
}
