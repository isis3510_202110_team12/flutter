import 'package:discountella/controllers/user_controller.dart';
import 'package:discountella/models/user.dart';
import 'package:discountella/services/analytics_service.dart';
import 'package:discountella/services/user_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginRegisterController extends GetxController {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final AnalyticsService _analyticsService = AnalyticsService();

  final formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final nameController = TextEditingController();
  final lastNameController = TextEditingController();
  final documentNumberController = TextEditingController();

  DateTime birthdate;
  String selectedType;
  String selectedGender;
  bool success;
  String userEmail;

  void dispose() {
    // Clean up the controller when the Widget is disposed
    emailController.clear();
    passwordController.clear();
    super.refresh();
  }

  void register() async {
    final User user = (await _auth.createUserWithEmailAndPassword(
      email: emailController.text,
      password: passwordController.text,
    ))
        .user;
    if (user != null) {
      Uid _uid = Uid(
        email: emailController.text,
        password: passwordController.text,
        documentType: selectedType,
        documentNumber: int.parse(documentNumberController.text),
      );

      UserData _user = UserData(
          name: nameController.text,
          email: emailController.text,
          lastName: lastNameController.text,
          gender: selectedGender,
          birthdate: DateTime.now());
      if (await UserServices().addUser(_uid, _user)) {
        Get.put(UserController()).user = _user;
      }
      String userId = _uid.documentType + _uid.documentNumber.toString();
      await _analyticsService.setUserProperties(userId: userId);
      await _analyticsService.logSignup();
      success = true;
      print('Registro Ok');
      Future.delayed(
        Duration(seconds: 2),
        () {
          Get.toNamed("/menu");
        },
      );
      userEmail = user.email;
    } else {
      success = false;
    }
  }
}
