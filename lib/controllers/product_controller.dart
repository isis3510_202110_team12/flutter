import 'package:discountella/models/product.dart';
import 'package:discountella/services/product_service.dart';
import 'package:get/state_manager.dart';

class ProductController extends GetxController {
  RxList<Product> productList = RxList<Product>();
  Category activeCategory;
  List<Product> get products => productList.value;
  bool _loading = true;

  bool get loading => _loading;

  @override
  void onInit() async {
    super.onInit();
    _loading = false;
    productList.bindStream(ProductService().productStream());
    update();
  }
}
