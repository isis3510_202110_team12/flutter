import 'package:discountella/models/market.dart';
import 'package:discountella/services/market_service.dart';
import 'package:get/get.dart';

class MarketController extends GetxController {
  Rx<Market> _market = Market().obs;

  Market get market => _market.value;

  RxList<Market> marketList = RxList<Market>();

  List<Market> get markets => marketList.value;

  set market(Market value) {
    this._market.value = value;
  }

  bool _loading = true;

  bool get loading => _loading;
  @override
  void onInit() {
    super.onInit();
    marketList.bindStream(MarketService().marketStream());
  }
}
