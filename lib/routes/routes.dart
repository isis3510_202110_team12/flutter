import 'package:discountella/views/add_list_page.dart';
import 'package:discountella/views/add_product_page.dart';
import 'package:discountella/views/geo_page.dart';
import 'package:discountella/views/home_page.dart';
import 'package:discountella/views/list_detail_page.dart';
import 'package:discountella/views/login_page.dart';
import 'package:discountella/views/map_page.dart';
import 'package:discountella/views/market_detail.dart';
import 'package:discountella/views/menu_page.dart';
import 'package:discountella/views/product_list_page.dart';
import 'package:discountella/views/product_page.dart';
import 'package:discountella/views/register_page.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';
import '../views/list_page.dart';

routes() => [
      GetPage(name: "/registration", page: () => RegisterLoginPage()),
      GetPage(name: "/home", page: () => HomePage()),
      GetPage(name: "/loginpage", page: () => LoginPage()),
      GetPage(name: "/menu", page: () => MenuPage()),
      GetPage(name: "/product", page: () => ProductPage()),
      GetPage(name: "/map", page: () => MapPage()),
      GetPage(name: "/list", page: () => ListPage()),
      GetPage(name: "/listProducts", page: () => ProductListPage()),
      GetPage(name: "/listDetail", page: () => ListDetailPage()),
      GetPage(name: "/addProduct", page: () => AddProductPage()),
      GetPage(name: "/addList", page: () => AddListPage()),
      GetPage(name: "/geo", page: () => GeoPage()),
      GetPage(name: "/marketDetail", page: () => MarketDetailPage()),
    ];
