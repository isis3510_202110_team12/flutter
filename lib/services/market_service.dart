import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:discountella/models/market.dart';
import 'package:geoflutterfire/geoflutterfire.dart';

class MarketService {
  final geo = Geoflutterfire();
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  Stream<List<Market>> marketStream() {
    return _firestore
        .collection('Market')
        .snapshots()
        .map((QuerySnapshot query) {
      List<Market> retVal = [];
      query.docs.forEach((element) {
        retVal.add(Market.fromDocumentSnapshot(element));
      });

      return retVal;
    });
  }

  Stream<List<DocumentSnapshot>> marketNotification(
      double latitude, double longitude) {
    var ref = _firestore.collection('Market');
    GeoFirePoint center = geo.point(latitude: latitude, longitude: longitude);
    var rad = 1.0;
    Stream<List<DocumentSnapshot>> p = geo
        .collection(collectionRef: ref)
        .within(
            center: center, radius: rad, field: 'position', strictMode: true);
    return p;
  }
}
