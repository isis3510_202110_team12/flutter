import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:discountella/models/product.dart';
import 'dart:math';
import 'dart:io';

class ProductService {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseStorage _storage = FirebaseStorage.instance;

  Stream<List<Product>> productStream() {
    return _firestore
        .collection('Product')
        .snapshots()
        .map((QuerySnapshot query) {
      List<Product> retVal = [];
      query.docs.forEach((element) {
        retVal.add(Product.fromDocumentSnapshot(element));
      });
      return retVal;
    });
  }

  Stream<List<Product>> listProduct(List<String> np) {
    return _firestore
        .collection('Product')
        .where(FieldPath.documentId, whereIn: np)
        .snapshots()
        .map((QuerySnapshot query) {
      List<Product> retVal = [];
      query.docs.forEach((element) {
        retVal.add(Product.fromDocumentSnapshot(element));
      });
      return retVal;
    });
  }

  Future<Product> getProduct(String uid) async {
    try {
      DocumentSnapshot _doc =
          await _firestore.collection("Product").doc(uid).get();
      return Product.fromDocumentSnapshot(_doc);
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  Stream<List<Product>> listnotProduct(List<String> np) {
    if (np != null && np.length > 0) {
      return _firestore
          .collection('Product')
          .where(FieldPath.documentId, whereNotIn: np)
          .snapshots()
          .map((QuerySnapshot query) {
        List<Product> retVal = [];
        query.docs.forEach((element) {
          retVal.add(Product.fromDocumentSnapshot(element));
        });
        return retVal;
      });
    }
    return _firestore
        .collection('Product')
        .snapshots()
        .map((QuerySnapshot query) {
      List<Product> retVal = [];
      query.docs.forEach((element) {
        retVal.add(Product.fromDocumentSnapshot(element));
      });
      return retVal;
    });
  }

  Future<bool> existProduct(String name, String brand, String units) async {
    try {
      var snapshots = _firestore
          .collection('Product')
          .where('name', isEqualTo: name)
          .where('brand', isEqualTo: brand)
          .where('unit', isEqualTo: units)
          .snapshots();

      if ((await snapshots.first).docs.length > 0) {
        return true;
      }
      return false;
    } catch (err) {
      print(err);
      return false;
    }
  }

  Future<bool> addProduct(Product product, File image) async {
    try {
      var random = new Random();
      TaskSnapshot storage = await _storage
          .ref()
          .child("Images/product_img_" +
              (random.nextInt(1000000) + 1000000).toString() +
              "." +
              (random.nextInt(1000000) + 1000000).toString() +
              "_" +
              new DateTime.now().microsecondsSinceEpoch.toString())
          .putFile(image);

      product.image = await storage.ref.getDownloadURL();
      product.categories = ["trCdDPLnFhExFpU5Sp8D"];

      await _firestore.collection('Product').add(product.toJson());
      return true;
    } catch (err) {
      print(err);
      return false;
    }
  }
}
