import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:discountella/models/lista.dart';
import '../models/lista.dart';

class ListService {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  Stream<List<Lista>> listStream(String uid) {
    return _firestore
        .collection('List')
        .where("idUser", isEqualTo: uid)
        .snapshots()
        .map((QuerySnapshot query) {
      List<Lista> retVal = [];
      query.docs.forEach((element) {
        retVal.add(Lista.fromDocumentSnapshot(element));
      });
      return retVal;
    });
  }

  Future<Lista> getList(String uid) async {
    try {
      DocumentSnapshot _doc =
          await _firestore.collection("List").doc(uid).get();
      return Lista.fromDocumentSnapshot(_doc);
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  Future<bool> existList(String title, String userId) async {
    try {
      var snapshots =  _firestore
          .collection('List')
          .where('title', isEqualTo: title)
          .where('idUser', isEqualTo: userId)
          .snapshots();

      if((await snapshots.first).docs.length > 0) {
        return true;
      }
      return false;
    }
    catch (err) {
      print(err);
      return false;
    }
  }

  Future<void> addProduct(String listId, Lista lista) async {
    try {
      await _firestore.collection('List').doc(listId).set(lista.toJson());
    } catch (err) {
      print(err);
    }
  }

  Future<bool> addList(Lista lista) async {
    try {
      await _firestore.collection('List').add(lista.toJson());
      return true;
    }
    catch(err) {
      print(err);
      return false;
    }
  }

  deleteOne(String uid) {
    _firestore.collection('List').doc(uid).delete();
  }
}
