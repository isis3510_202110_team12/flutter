import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:discountella/models/user.dart';

class UserServices {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  final CollectionReference users =
      FirebaseFirestore.instance.collection('Users');

  Future<bool> addUser(Uid uid, UserData userdata) async {
    try {
      String userId = uid.documentType + uid.documentNumber.toString();
      print(userId);
      await _firestore.collection('Users').doc(userId).set(userdata.toJson());
      return true;
    } catch (err) {
      print(err);
      return false;
    }
  }

  Future<String> getIdUserByEmail(String email) async {
    try {
      return (await users.where('email', isEqualTo: email).get())
          .docs[0]
          .reference
          .id;
    } catch (e) {
      throw (e);
    }
  }

  Future<UserData> getUser(String uid) async {
    try {
      DocumentSnapshot _doc =
          await _firestore.collection('Users').doc(uid).get();
      return UserData.fromDocumentSnapshot(_doc);
    } catch (e) {
      print(e);
      rethrow;
    }
  }
}
