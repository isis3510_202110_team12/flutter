import 'package:discountella/models/product.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/foundation.dart';

import '../models/market.dart';

class AnalyticsService {
  final FirebaseAnalytics _analytics = FirebaseAnalytics();

  FirebaseAnalyticsObserver getAnalyticsObserver() =>
      FirebaseAnalyticsObserver(analytics: _analytics);

  Future setUserProperties({@required String userId}) async {
    await _analytics.setUserId(userId);
  }

  Future logLogin() async {
    await _analytics.logLogin(loginMethod: 'email');
  }

  Future logSignup() async {
    await _analytics.logSignUp(signUpMethod: 'email');
  }

  Future logMarketViews({@required Market market}) async {
    await _analytics.logEvent(
        name: 'Supermercado_Consultado',
        parameters: <String, dynamic>{
          'market_name': market.name,
          'market_id': market.uid,
        });
  }

  Future logProductosCreados({@required Product producto}) async {
    await _analytics
        .logEvent(name: 'ProductosCreados', parameters: <String, dynamic>{
      'product_name': producto.name,
      'product_id': producto.uid,
      'product_brand': producto.brand,
    });
  }

  Future logVerTodosProductos() async {
    await _analytics.logEvent(name: 'Viendo_Productos');
  }

  Future logRecomendar() async {
    await _analytics.logEvent(name: 'Recomendar_feature');
  }

  Future logAgregarLista() async {
    await _analytics.logEvent(name: 'Agregar_lista');
  }

  Future logProductosagragadosLista({@required Product producto}) async {
    await _analytics.logEvent(
        name: 'ProductosAgragados_Lista',
        parameters: <String, dynamic>{
          'product_name': producto.name,
          'product_id': producto.uid,
        });
  }

  Future logContextAware() async {
    await _analytics.logEvent(name: 'ContextAware_feature');
  }

  Future logNearbyMarkets() async {
    await _analytics.logEvent(name: 'NearbyMarkets_Notification_feature');
  }

  Future logScreens({@required String name}) async {
    await _analytics.setCurrentScreen(screenName: name);
  }
}
