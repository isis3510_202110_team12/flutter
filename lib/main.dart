import 'dart:math';
import 'package:discountella/routes/routes.dart';
import 'package:discountella/views/home_page.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:discountella/services/analytics_service.dart';

import 'controllers/location_controller.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  FirebaseAnalytics analytics = FirebaseAnalytics();
  final LocationController locationController =
      Get.put<LocationController>(LocationController());
  @override
  Widget build(BuildContext context) {
    locationController.notification();

    return GetMaterialApp(
      theme: ThemeData(
        primaryColor: Color(0xff582630),
        primarySwatch: generateMaterialColor(Color(0xffA54657)),
        scaffoldBackgroundColor: Color(0xfff4f1d5),
      ),
      debugShowCheckedModeBanner: false,
      home: HomePage(),
      navigatorKey: Get.key,
      getPages: routes(),
      navigatorObservers: [AnalyticsService().getAnalyticsObserver()],
    );
  }

  MaterialColor generateMaterialColor(Color color) {
    return MaterialColor(color.value, {
      50: tintColor(color, 0.5),
      100: tintColor(color, 0.4),
      200: tintColor(color, 0.3),
      300: tintColor(color, 0.2),
      400: tintColor(color, 0.1),
      500: tintColor(color, 0),
      600: tintColor(color, -0.1),
      700: tintColor(color, -0.2),
      800: tintColor(color, -0.3),
      900: tintColor(color, -0.4),
    });
  }

  int tintValue(int value, double factor) =>
      max(0, min((value + ((255 - value) * factor)).round(), 255));

  Color tintColor(Color color, double factor) => Color.fromRGBO(
      tintValue(color.red, factor),
      tintValue(color.green, factor),
      tintValue(color.blue, factor),
      1);
}
