import 'package:flutter/material.dart';

class IconsMenu {
  static const items = <IconMenu>[
    detail,
    recomendar,
    agregar,
    remove,
  ];

  static const detail = IconMenu(
    text: 'Detail',
    icon: Icons.remove_red_eye_outlined,
  );

  static const recomendar = IconMenu(
    text: 'Recomendar',
    icon: Icons.list_alt,
  );

  static const agregar = IconMenu(
    text: 'Agregar',
    icon: Icons.playlist_add_outlined,
  );

  static const remove = IconMenu(
    text: 'Remove',
    icon: Icons.delete,
  );
}

class IconMenu {
  final String text;
  final IconData icon;

  const IconMenu({
    @required this.text,
    @required this.icon,
  });
}
