import 'package:discountella/controllers/login_controller.dart';
import 'package:discountella/controllers/network_controller.dart';
import 'package:discountella/controllers/user_controller.dart';
import 'package:discountella/services/analytics_service.dart';
import 'package:discountella/services/user_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MenuPage extends StatelessWidget {
  final controller = Get.put(LoginController());
  final NetworkController _networkController = Get.find<NetworkController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        title: GetX<UserController>(
          initState: (_) async {
            if (UserController().user == null) {
              var userid =
                  await UserServices().getIdUserByEmail(controller.user.email);
              UserController().user = await UserServices().getUser(userid);
            }
          },
          builder: (_) {
            if (_.user.name != null) {
              return Text("Welcome " + _.user.name);
            } else {
              return Text("loading...");
            }
          },
        ),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {
              controller.signOut();
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 40, bottom: 40),
              child: Row(children: <Widget>[
                Expanded(
                    child: Divider(
                  color: Color(0xff582630),
                  height: 20,
                  thickness: 5,
                  indent: 20,
                  endIndent: 20,
                )),
                Text(
                  'Home',
                  style: TextStyle(
                    fontSize: 24,
                  ),
                ),
                Expanded(
                    child: Divider(
                  color: Color(0xff582630),
                  height: 20,
                  thickness: 5,
                  indent: 20,
                  endIndent: 20,
                )),
              ]),
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    minimumSize: Size(30.0, 60.0),
                  ),
                  onPressed: () async {
                    if (_networkController.connectionStatus.value != 0)
                      Get.toNamed("/map");
                    else
                      Get.snackbar(
                          "Network Error", "Failed to get network connection");
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.location_pin,
                        size: 26.0,
                      ),
                      Text('SUPERMERCADOS', textAlign: TextAlign.right),
                    ],
                  )),
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    minimumSize: Size(30.0, 60.0),
                  ),
                  onPressed: () async {
                    if (_networkController.connectionStatus.value != 0)
                      Get.toNamed("/list");
                    else
                      Get.snackbar(
                          "Network Error", "Failed to get network connection");
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.list_alt_outlined,
                        size: 26.0,
                      ),
                      Text('LISTA COMPRAS', textAlign: TextAlign.right),
                    ],
                  )),
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    minimumSize: Size(30.0, 60.0),
                  ),
                  onPressed: () async {
                    if (_networkController.connectionStatus.value != 0) {
                      _logProductos();
                      Get.toNamed("/product");
                    } else
                      Get.snackbar(
                          "Network Error", "Failed to get network connection");
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.local_grocery_store,
                        size: 26.0,
                      ),
                      Text('PRODUCTOS', textAlign: TextAlign.right),
                    ],
                  )),
            ),
          ],
        ),
      ),
    );
  }

  final AnalyticsService _analyticsService = AnalyticsService();
  void _logProductos() async {
    await _analyticsService.logVerTodosProductos();
  }
}
