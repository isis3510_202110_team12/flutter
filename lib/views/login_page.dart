import 'package:discountella/controllers/login_controller.dart';
import 'package:discountella/controllers/network_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:get/get.dart';

class LoginPage extends StatelessWidget {
  final NetworkController _networkController =
      Get.put<NetworkController>(NetworkController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Discountella",
          style: TextStyle(fontSize: 20),
        ),
      ),
      body: GetBuilder<LoginController>(
        init: LoginController(),
        builder: (_) {
          return SingleChildScrollView(
            child: Form(
              key: _.formKey,
              child: Card(
                color: Color(0xFFF1A66A),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(top: 16.0),
                        child: const Text(
                          'Login Page',
                          style: TextStyle(fontWeight: FontWeight.bold , fontSize: 20),
                        ),
                        alignment: Alignment.center,
                      ),
                      TextFormField(
                        controller: _.emailController,
                        decoration: const InputDecoration(labelText: 'Email'),
                        validator: (String value) {
                          if (value.isEmpty) return 'Please enter some text';
                          return null;
                        },
                      ),
                      TextFormField(
                        controller: _.passwordController,
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                        ],
                        decoration:
                            const InputDecoration(labelText: 'Password'),
                        validator: (String value) {
                          if (value.isEmpty)
                            return 'Please enter some text or numbers';
                          return null;
                        },
                        obscureText: true,
                      ),
                      Container(
                          padding: const EdgeInsets.only(top: 16.0),
                          alignment: Alignment.center,
                          child: SignInButton(Buttons.Email, text: "Sign In",
                              onPressed: () async {
                            if (_networkController.connectionStatus.value != 0)
                              _.signInWithEmailAndPassword();
                            else
                              Get.snackbar("Network Error",
                                  "Failed to get network connection");
                          })),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
