import 'package:flutter/material.dart';
import 'package:flutter_signin_button/button_builder.dart';
import 'package:get/get.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        title: Text('Discountella'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 100,
          ),
          Container(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(30),
              child: Image.asset(
                'assets/images/icon.png',
                width: 200,
                fit: BoxFit.scaleDown,
              ),
            ),
            padding: const EdgeInsets.all(16),
            alignment: Alignment.center,
          ),
          Container(
            child: SignInButtonBuilder(
              icon: Icons.verified_user,
              backgroundColor: Color(0xffA54657),
              text: 'Sign In',
              onPressed: () => Get.toNamed("/loginpage"),
            ),
            padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
            alignment: Alignment.center,
          ),
          Container(
            child: TextButton(
              style: TextButton.styleFrom(
                textStyle: const TextStyle(fontSize: 12),
              ),
              onPressed: () => Get.toNamed("/registration"),
              child: const Text('Dont have an account? Sign up',
                  style: TextStyle(fontSize: 12, color: Colors.blue)),
            ),
            alignment: Alignment.center,
          ),
        ],
      ),
    );
  }
}
