import 'package:discountella/Helpers/icon_menu.dart';
import 'package:discountella/controllers/list_controller.dart';
import 'package:discountella/controllers/network_controller.dart';
import 'package:discountella/services/analytics_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListPage extends StatelessWidget {
  final NetworkController _networkController = Get.find<NetworkController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Listas",
          style: TextStyle(fontSize: 20),
        ),
      ),
      body: GetBuilder<ListController>(
          init: ListController(),
          builder: (_) {
            if (_.loading) {
              return CircularProgressIndicator();
            }
            return Obx(
              () => ListView.builder(
                itemCount: _.listas.length,
                itemBuilder: (context, index) {
                  return Card(
                      color: Color(0xFFF1A66A),
                      child: ListTile(
                          title: Text(_.listas[index].title),
                          subtitle: Text("Número de Productos: " +
                              _.listas[index].products.length.toString() +
                              "\nFecha de creacion: " +
                              _.listas[index].date),
                          isThreeLine: true,
                          trailing: new PopupMenuButton(
                            onSelected: (value) async {
                              switch (value) {
                                case IconsMenu.detail:
                                  _.lista = _.listas[index];
                                  Get.toNamed("/listDetail");
                                  break;
                                case IconsMenu.recomendar:
                                  _logRecomendar();
                                  var suggested =
                                      _.getSuggest(_.listas[index].products);
                                  Get.dialog(AlertDialog(
                                    title: const Text('Recomendación'),
                                    content: SingleChildScrollView(
                                      child: ListBody(
                                        children: <Widget>[
                                          Text(
                                              'Tu recomendación en función de los productos de tu lista es:'),
                                          Padding(
                                            padding: const EdgeInsets.all(16.0),
                                          ),
                                          Text(await suggested,
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                height: 1,
                                                fontSize: 20,
                                              ),
                                              textAlign: TextAlign.center),
                                        ],
                                      ),
                                    ),
                                    actions: <Widget>[
                                      TextButton(
                                        child: const Text('Vale'),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ],
                                  ));
                                  break;
                                case IconsMenu.agregar:
                                  _.lista = _.listas[index];
                                  Get.toNamed("/listProducts");
                                  break;
                                case IconsMenu.remove:
                                  _.deleteList(_.listas[index].uid);
                                  break;
                              }
                            },
                            itemBuilder: (BuildContext context) =>
                                IconsMenu.items
                                    .map((item) => PopupMenuItem<IconMenu>(
                                          value: item,
                                          child: ListTile(
                                            contentPadding: EdgeInsets.zero,
                                            leading: Icon(item.icon,
                                                color: Color(0xFFA54657)),
                                            title: Text(item.text,
                                                style: TextStyle(
                                                    color: Color(0xFFA54657))),
                                          ),
                                        ))
                                    .toList(),
                          )));
                },
              ),
            );
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          if (_networkController.connectionStatus.value != 0) {
            Get.toNamed("/addList");
          } else {
            Future.delayed(
              Duration(seconds: 1),
              () {
                Get.defaultDialog(
                  title: "No tienes acceso a internet",
                  radius: 20,
                  backgroundColor: Colors.grey,
                  middleText: "Puede que algunos datos no están actualizados",
                );
              },
            );
            Get.toNamed("/addList");
          }
        },
        child: const Icon(Icons.add),
        backgroundColor: Color(0xffA54657),
      ),
    );
  }

  final AnalyticsService _analyticsService = AnalyticsService();

  void _logRecomendar() async {
    await _analyticsService.logRecomendar();
  }
}
