import 'package:discountella/controllers/add_list_controller.dart';
import 'package:discountella/controllers/network_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddListPage extends StatelessWidget {
  final NetworkController _networkController = Get.find<NetworkController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Discountella",
          style: TextStyle(fontSize: 20),
        ),
      ),
      body: GetBuilder<AddListController>(
        init: AddListController(),
        builder: (_) {
          if (_.loading) {
            return CircularProgressIndicator();
          }
          return SingleChildScrollView(
            child: Form(
              key: _.formKey,
              child: Card(
                color: Color(0xFFF1A66A),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(top: 16.0),
                        child: const Text(
                          'Añade una nueva lista',
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                        alignment: Alignment.center,
                      ),
                      TextFormField(
                        controller: _.titleController,
                        decoration: const InputDecoration(labelText: 'Título'),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor ingresa un título';
                          }
                          return null;
                        },
                      ),
                      Container(
                        padding: const EdgeInsets.only(top: 16.0),
                        alignment: Alignment.center,
                        child: ElevatedButton(
                            child: Text('Agregar lista'),
                            onPressed: () async {
                              if (_networkController.connectionStatus.value != 0) {
                                if (_.formKey.currentState.validate()) {
                                  _.addList();
                                  Get.back();
                                }
                              } else {
                                Get.defaultDialog(
                                    title: "Discountella Error",
                                    content: Text("No tienes acceso a internet"));
                              }}
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: Text(_.success == null
                            ? ''
                            : (_.success
                            ? 'Successfully registered new product ' +
                            _.toString()
                            : 'Add product failed')),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
