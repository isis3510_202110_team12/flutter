import 'package:discountella/controllers/add_product_controller.dart';
import 'package:discountella/controllers/network_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class AddProductPage extends StatelessWidget {
  final NetworkController _networkController = Get.find<NetworkController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Agregar Producto",
          style: TextStyle(fontSize: 20),
        ),
      ),
      body: GetBuilder<AddProductController>(
        init: AddProductController(),
        builder: (_) {
          if (_.loading) {
            return CircularProgressIndicator();
          }
          return SingleChildScrollView(
            child: Form(
              key: _.formKey,
              child: Card(
                color: Color(0xFFF1A66A),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(top: 16.0),
                        alignment: Alignment.center,
                        child: const Text(
                          'Añade una nuevo producto',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                      ),
                      TextFormField(
                        controller: _.nameController,
                        decoration: const InputDecoration(labelText: 'Nombre'),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor ingresa un nombre';
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        controller: _.brandController,
                        decoration: const InputDecoration(labelText: 'Marca'),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor ingresa una marca';
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        controller: _.imageController,
                        decoration: const InputDecoration(labelText: 'Imagen'),
                        readOnly: true,
                        onTap: () => _.uploadImage(),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor ingresa una imagen';
                          }
                          return null;
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                      ),
                      DropdownButtonFormField(
                        value: _.unitController,
                        hint: Text("Unidades"),
                        items: <String>['Kg', 'Lb', 'Gr', 'Paquete', "Unidad"]
                            .map((String value) {
                          return new DropdownMenuItem<String>(
                            value: value,
                            child: new Text(value),
                          );
                        }).toList(),
                        validator: (value) => value == null
                            ? 'Por favor selecciona la unidad'
                            : null,
                        onChanged: (newValue) async {
                          _.unitController = newValue;
                        },
                      ),
                      TextFormField(
                        controller: _.amountController,
                        decoration:
                            const InputDecoration(labelText: 'Cantidad'),
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor ingresa una cantidad';
                          }
                          return null;
                        },
                      ),
                      Container(
                        padding: const EdgeInsets.only(top: 16.0),
                        alignment: Alignment.center,
                        child: ElevatedButton(
                          child: Text('Agregar producto'),
                          onPressed: () async {
                            if (_networkController.connectionStatus.value != 0) {
                              if (_.formKey.currentState.validate()) {
                                _.addProduct();
                                Get.back();
                              }
                            } else {
                              Get.defaultDialog(
                                  title: "Discountella Error",
                                  content: Text("No tienes acceso a internet"));
                            }
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: Text(_.success == null
                            ? ''
                            : (_.success
                                ? 'Successfully registered new product ' +
                                    _.toString()
                                : 'Add product failed')),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
