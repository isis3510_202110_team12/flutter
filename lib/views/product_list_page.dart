import 'package:cached_network_image/cached_network_image.dart';
import 'package:discountella/controllers/list_controller.dart';
import 'package:discountella/controllers/product_list_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductListPage extends StatelessWidget {
  final controller = Get.find<ListController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Productos",
          style: TextStyle(fontSize: 20),
        ),
      ),
      body: GetBuilder<ProductListController>(
        init: ProductListController(),
        builder: (_) {
          if (_.loading) {
            return CircularProgressIndicator();
          }
          return Obx(
            () => ListView.builder(
              itemCount: _.products.length,
              itemBuilder: (context, index) {
                return Card(
                  color: Color(0xFFF1A66A),
                  child: ListTile(
                    leading: buildImage(_.products[index].image),
                    title: Text(_.products[index].name),
                    subtitle: Text(_.products[index].brand),
                    trailing: Icon(Icons.add_to_photos),
                    isThreeLine: true,
                    onTap: () => {
                      controller.addProduct(_.products[index].uid),
                      Get.back()
                    },
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }

  Widget buildImage(String image) => ClipRRect(
    borderRadius: BorderRadius.circular(12),
    child: CachedNetworkImage(
      imageUrl: image,
      progressIndicatorBuilder: (context, url, downloadProgress) =>
          CircularProgressIndicator(value: downloadProgress.progress),
      errorWidget: (context, url, error) => Icon(Icons.error),
      height: 50,
      width: 50,
      fit: BoxFit.cover,
    ),
  );
}
