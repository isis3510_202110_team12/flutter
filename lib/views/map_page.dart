import 'package:discountella/controllers/market_controller.dart';
import 'package:discountella/models/market.dart';
import 'package:discountella/services/analytics_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:get/get.dart';

class MapPage extends StatefulWidget {
  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  final MarketController _marketController =
      Get.put<MarketController>(MarketController());
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Position currentPosition;
  GoogleMapController _mapController;

  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      _mapController = controller;
      _showHome();
    });
  }

  Future<void> _showHome() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    currentPosition = position;
    _mapController.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
        target: LatLng(position.latitude, position.longitude),
        zoom: 15.0,
      ),
    ));
    _updateMarkers(_marketController.markets);
  }

  void _addMarker(Market market) {
    double lat = market.point.latitude;
    double lng = market.point.longitude;
    final id = MarkerId(lat.toString() + lng.toString());
    final _marker = Marker(
      markerId: id,
      position: LatLng(lat, lng),
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
      infoWindow: InfoWindow(
        title: market.name,
        snippet: '$lat,$lng',
        onTap: () => {
          _marketController.market = market,
          Get.toNamed("/marketDetail"),
          _logMarket(market)
        },
      ),
    );
    setState(() {
      markers[id] = _marker;
    });
  }

  void _updateMarkers(List<Market> markets) {
    markets.forEach((Market market) {
      _addMarker(market);
    });
  }

  final AnalyticsService _analyticsService = AnalyticsService();
  void _logMarket(Market market) async {
    await _analyticsService.logMarketViews(
      market: market,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Supermercados",
            style: TextStyle(fontSize: 20),
          ),
        ),
        body: GoogleMap(
          onMapCreated: _onMapCreated,
          myLocationButtonEnabled: true,
          myLocationEnabled: true,
          initialCameraPosition: CameraPosition(
            target: LatLng(4.6097100, -74.0817500),
            zoom: 15.0,
          ),
          markers: Set<Marker>.of(markers.values),
        ));
  }
}
