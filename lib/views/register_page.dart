import 'package:discountella/controllers/network_controller.dart';
import 'package:discountella/controllers/registration_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterLoginPage extends StatelessWidget {
  final NetworkController _networkController =
  Get.put<NetworkController>(NetworkController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Discountella",
          style: TextStyle(fontSize: 20),
        ),
      ),
      body: GetBuilder<LoginRegisterController>(
        init: LoginRegisterController(),
        builder: (_) {
          return SingleChildScrollView(
            child: Form(
              key: _.formKey,
              child: Card(
                color: Color(0xFFF1A66A),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(top: 16.0),
                        alignment: Alignment.center,
                        child: const Text(
                          'Registro',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                      ),
                      TextFormField(
                        controller: _.emailController,
                        decoration: const InputDecoration(labelText: 'Correo'),
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Por favor ingresa tu correo';
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        controller: _.nameController,
                        decoration: const InputDecoration(labelText: 'Nombre'),
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Por favor ingresa tu nombre';
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        controller: _.lastNameController,
                        decoration:
                        const InputDecoration(labelText: 'Apellido'),
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Por favor ingresa tu apellido';
                          }
                          return null;
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.all(5),
                      ),
                      DropdownButtonFormField(
                        value: _.selectedGender,
                        hint: Text("Género"),
                        items: <String>["Hombre", "Mujer", "Otro"]
                            .map((String value) {
                          return new DropdownMenuItem<String>(
                            value: value,
                            child: new Text(value),
                          );
                        }).toList(),
                        validator: (value) => value == null
                            ? 'Por favor selecciona tu género'
                            : null,
                        onChanged: (newValue) async {
                          _.selectedGender= newValue;
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.all(5),
                      ),
                      DropdownButtonFormField(
                        value: _.selectedType,
                        hint: Text("Tipo de documento"),
                        items: <String>["CC", "CE", "TI", "PA"]
                            .map((String value) {
                          return new DropdownMenuItem<String>(
                            value: value,
                            child: new Text(value),
                          );
                        }).toList(),
                        validator: (value) => value == null
                            ? 'Por favor selecciona tu tipo de documento'
                            : null,
                        onChanged: (newValue) async {
                          _.selectedType = newValue;
                        },
                      ),
                      TextFormField(
                        controller: _.documentNumberController,
                        decoration: const InputDecoration(
                            labelText: 'Numero Documento'),
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Por favor ingresa tu número de documento';
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        controller: _.passwordController,
                        decoration:
                        const InputDecoration(labelText: 'Password'),
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Por favor ingresa tu contraseña';
                          }
                          else if(value.length < 6) {
                            return 'La contraseña debe tener al menos 6 caracteres';
                          }
                          return null;
                        },
                        obscureText: true,
                      ),
                      Container(
                        padding: const EdgeInsets.only(top: 16.0),
                        alignment: Alignment.center,
                        child: ElevatedButton(
                          child: Text('Registro'),
                          onPressed: () async {
                            if (_networkController.connectionStatus.value != 0) {
                              if (_.formKey.currentState.validate()) {
                                _.register();
                              }
                            }else  {
                              Get.snackbar("Network Error",
                                  "Failed to get network connection");
                            }
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: Text(_.success == null
                            ? ''
                            : (_.success
                            ? 'Successfully registered ' + _.userEmail
                            : 'Registration failed')),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
