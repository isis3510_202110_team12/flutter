import 'package:cached_network_image/cached_network_image.dart';
import 'package:discountella/controllers/network_controller.dart';
import 'package:discountella/controllers/product_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductPage extends StatelessWidget {
  final NetworkController _networkController = Get.find<NetworkController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Productos",
          style: TextStyle(fontSize: 20),
        ),
      ),
      body: GetBuilder<ProductController>(
        init: ProductController(),
        builder: (_) {
          if (_.loading) {
            return CircularProgressIndicator();
          }
          return Obx(
            () => ListView.builder(
              itemCount: _.products.length,
              itemBuilder: (context, index) {
                return Card(
                  color: Color(0xFFF1A66A),
                  child: ListTile(
                    leading: buildImage(_.products[index].image),
                    title: Text(_.products[index].name),
                    subtitle: Text(_.products[index].brand),
                    isThreeLine: true,
                  ),
                );
              },
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          if (_networkController.connectionStatus.value != 0) {
            Get.toNamed("/addProduct");
          } else {
            Future.delayed(
              Duration(seconds: 1),
              () {
                Get.defaultDialog(
                  title: "No tienes acceso a internet",
                  radius: 20,
                  backgroundColor: Colors.grey,
                  middleText: "Puede que algunos datos no están actualizados",
                );
              },
            );
            Get.toNamed("/addProduct");
          }
        },
        child: const Icon(Icons.add),
        backgroundColor: Color(0xffA54657),
      ),
    );
  }

  Widget buildImage(String image) => ClipRRect(
        borderRadius: BorderRadius.circular(12),
        child: CachedNetworkImage(
          imageUrl: image,
          progressIndicatorBuilder: (context, url, downloadProgress) =>
              CircularProgressIndicator(value: downloadProgress.progress),
          errorWidget: (context, url, error) => Icon(Icons.error),
          height: 50,
          width: 50,
          fit: BoxFit.cover,
        ),
      );
}
