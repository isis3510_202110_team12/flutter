import 'package:cached_network_image/cached_network_image.dart';
import 'package:discountella/controllers/market_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MarketDetailPage extends StatelessWidget {
  final MarketController _marketController = Get.find<MarketController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Discountella",
          style: TextStyle(fontSize: 20),
        ),
      ),
      body: Card(
        color: Color(0xFFA54657),
        child: ListTile(
          leading: CachedNetworkImage(
            imageUrl: _marketController.market.image,
            progressIndicatorBuilder: (context, url, downloadProgress) =>
                CircularProgressIndicator(value: downloadProgress.progress),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
          title: Text(_marketController.market.name),
          subtitle: Text(_marketController.market.chain),
          isThreeLine: true,
        ),
      ),
    );
  }
}
