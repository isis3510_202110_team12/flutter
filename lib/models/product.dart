import 'package:cloud_firestore/cloud_firestore.dart';

class Category {
  String description;
  String name;

  Category(
    this.description,
    this.name,
  );

  Category.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    description = documentSnapshot['description'];
    name = documentSnapshot['name'];
  }
}

class Product {
  String uid;
  int amount;
  String brand;
  List<dynamic> categories;
  String image;
  String name;
  String unit;

  Product(this.amount, this.brand, this.name, this.unit);

  Product.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    uid = documentSnapshot.id;
    amount = documentSnapshot['amount'];
    brand = documentSnapshot['brand'];
    categories = documentSnapshot['categories'];
    image = documentSnapshot['image'];
    name = documentSnapshot['name'];
    unit = documentSnapshot['unit'];
  }

  Map<String, dynamic> toJson() => {
        'amount': amount,
        'brand': brand,
        'categories': categories,
        'image': image,
        'name': name,
        'unit': unit
      };
}
