import 'package:cloud_firestore/cloud_firestore.dart';

class Market {
  String uid;
  String chain;
  String image;
  GeoPoint location;
  GeoPoint point;
  String name;

  Market({
    this.uid,
    this.chain,
    this.image,
    this.location,
    this.point,
    this.name,
  });

  Market.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    uid = documentSnapshot.id;
    chain = documentSnapshot['chain'];
    //location = documentSnapshot['location'];
    point = documentSnapshot['position']['geopoint'];
    image = documentSnapshot['image'];
    name = documentSnapshot['name'];
  }
}
