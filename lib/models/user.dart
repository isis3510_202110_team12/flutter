import 'package:cloud_firestore/cloud_firestore.dart';

class Uid {
  String email;
  String password;
  int documentNumber;
  String documentType;

  Uid({this.email, this.password, this.documentType, this.documentNumber});
}

class UserData {
  String uid;
  String name;
  String lastName;
  String email;
  DateTime birthdate;
  String gender;

  UserData({
    this.uid,
    this.name,
    this.lastName,
    this.email,
    this.birthdate,
    this.gender,
  });

  UserData.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    uid = documentSnapshot.id;
    name = documentSnapshot['name'];
    lastName = documentSnapshot['lastName'];
    email = documentSnapshot['email'];
    birthdate = DateTime.fromMillisecondsSinceEpoch(
        documentSnapshot['birthdate'] * 1000);
    gender = documentSnapshot['gender'];
  }

  Map<String, dynamic> toJson() => {
        'name': name,
        'lastName': lastName,
        'email': email,
        'birthdate': Timestamp.fromDate(birthdate).millisecondsSinceEpoch,
        'gender': gender
      };
}
