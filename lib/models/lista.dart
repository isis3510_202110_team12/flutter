import 'package:cloud_firestore/cloud_firestore.dart';

class Lista {
  String uid;
  String date;
  String idUser;
  List<dynamic> products;
  String title;

  Lista({this.date, this.idUser, this.title, this.products});

  Lista.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    uid = documentSnapshot.id;
    date = documentSnapshot['date'];
    idUser = documentSnapshot['idUser'];
    products = documentSnapshot['products'];
    title = documentSnapshot['title'];
  }

  Map<String, dynamic> toJson() =>
      {'date': date, 'idUser': idUser, 'title': title, 'products': products};
}
